import React from 'react';
import { IntlProvider } from 'react-intl';
import BrowserRouter from './Components/Router/Router';

const App = () => (
  <IntlProvider locale="en">
    <BrowserRouter />
  </IntlProvider>
);
export default App;
