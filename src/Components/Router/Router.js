import React from 'react';
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';
import { HOME, REGISTRATION } from '../../constants/RouteConstants';
import Home from '../Home';
import Registration from '../Registration/Registration';

const Router = () => (
  <BrowserRouter>
    <Switch>
      <Route exact path="/">
        <Redirect to={HOME} />
      </Route>
      <Route path={HOME} component={Home} />
      <Route path={REGISTRATION} component={Registration} />
    </Switch>
  </BrowserRouter>
);

export default Router;