import React from 'react';
import { FormattedMessage } from 'react-intl';
import { HOME, REGISTRATION } from '../../constants/RouteConstants';

const RouteMap = [
  {
    text: <FormattedMessage defaultMessage="HOME" description="navigation to home" id="menu.home" />,
    path: HOME
  },
  {
    text: <FormattedMessage defaultMessage="SIGN-UP" description="navigation to registration" id="menu.registration" />,
    path: REGISTRATION
  }
]

export default RouteMap