import React from 'react';
import { FormattedMessage } from 'react-intl';
import PropTypes from 'prop-types';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';

const Header = ({ onClose }) => (
  <>
    <IconButton onClick={onClose}>
      <CloseIcon style={{ color:"blue" }} />
    </IconButton>
    <span>
      <FormattedMessage defaultMessage="MENU" description="heading indicating menu options" id="menu.menu" />
    </span>
  </>
);

Header.propTypes = {
  onClose: PropTypes.func.isRequired
}

export default Header;