/* eslint-disable react/prop-types */
import React, { useState } from 'react';
import MenuIcon from '@material-ui/icons/Menu';
import IconButton from '@material-ui/core/IconButton';
import SideDrawer from './SideDrawer';
import styles from './Home.module.css'

const HamBurgerIcon = ({ onClick }) => (
  <div className={styles.btnClass} >
    <IconButton onClick={onClick}>
      <MenuIcon style={{ color:"blue" }} size="medium"/>
    </IconButton>
  </div>
);

const Home = () => {
  const [isDrawerOpen, setDrawerOpen] = useState(false);
  const drawerCloseHandler = () => setDrawerOpen(false);
  const drawerOpenHandler = () => setDrawerOpen(true);
  return (
  <>
    {!isDrawerOpen && <HamBurgerIcon onClick={drawerOpenHandler}/>}
    <SideDrawer onClose={drawerCloseHandler} isOpen={isDrawerOpen}/>
  </>
  );
};

export default Home;